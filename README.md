1) kubectl run web --image=gcr.io/google-samples/hello-app:1.0 --port=8080

2) kubectl expose deployment web --target-port=8080 --type=NodePort

3) gcloud compute addresses create kubernetes-ingress --global

4)
```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: basic-ingress
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "kubernetes-ingress"
spec:
  backend:
    serviceName: web
    servicePort: 8080
```

5) kubectl create -f ingress.yml